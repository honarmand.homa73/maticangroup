import { combineReducers, configureStore } from "@reduxjs/toolkit";

import clients from "./clientsSlice";

const rootReducer = combineReducers({
  clients,
});

const reducer = (state, action) => {
  return rootReducer(state, action);
};

const store = configureStore({ reducer });

export default store;