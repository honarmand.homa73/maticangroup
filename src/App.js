import ClientList from "./page/ClientList/ClientList";
import { BrowserRouter, Routes, Route} from "react-router-dom";
import i18n from "i18next";
import { useTranslation, initReactI18next } from "react-i18next";
import LanguageDetector from 'i18next-browser-languagedetector';
import HttpApi from 'i18next-http-backend';
import ProfileClients from "./page/ProfileClients/ProfileClients";

i18n
  .use(initReactI18next) 
  .use(LanguageDetector)
  .use(HttpApi)
  .init({       
    fallbackLng: "en",
    detection:{
      order: ['htmlTag', 'cookie', 'localStorage' , 'path', 'subdomain'],
      cache:['cookie'],
    },
  backend:{
    loadPath:'/assets/locales/{{lang}}/trabslation.json',
  },
  react:{useSuspense:false},
  });
function App() {
 const {t}=useTranslation()
  return (
    <div className="App">
     {/* <h2>{t('Welcome_to_React')}</h2>    */}
    <BrowserRouter>
      <Routes>
        <Route path="/" exact>
          <Route index element={<ClientList />} />
        </Route>
        <Route path="/:id">
          <Route index element={<ProfileClients />} />
        </Route>
      </Routes>
    </BrowserRouter>
      
    </div>
  );
}

export default App;
