import * as yup from "yup";

export const clientSchema = yup.object().shape({
  firstName: yup.string().required("Please enter your First name"),
  lastName: yup.string().required("Please enter your last name"),
  sendNotificationBy: yup.string().required("Please enter your sendNotificationBy"),
  mobile: yup.string().required("Please enter your mobile number"),
  preferredLang: yup.string().required("Please select preferred language"),
  Gender: yup.string().required('Please select your gender'),
  Referral: yup.string().required('Please select your referral'),
  Year: yup.string().required("Please select your birth year"),
  Month: yup.string().required("Please select your birthday Month"),
  Day: yup.string().required("Please select your birth year"),
  Area: yup.string().required("Please enter your Area"),
  Block: yup.string().required("Please enter your Block"),
  Street: yup.string().required("Please enter your Street"),
  Avenue: yup.string().required("Please enter your Avenue"),
  House: yup.string().required("Please enter your House number / Building / Apartment"),
});
