import { createTheme } from "@mui/material";
const baseColor = "#242BA8";
const secendColor = "#3D43CA";
const thirdColor = "#4048D6";
const chip1 = "#FCF3E4";
const chip2 = "#EAF4FF";
const chip3 = "#E4FAF7";
const chip4 = "#FFE7EB";
const chip5 = "#F6E3FF";
const chipText = "#0A1F44";
const boxSearch = "rgb(225 225 225 / 39%)";
const white = "#FFFFFF";
const black = "#00000";
const colorButton = "#8E98A9";
const textInformation = "#4E5D78";
const textMobile = "#4E5D78";
const theme = createTheme({
  typography: {
    fontFamily: [
      "Quicksand",
      "-apple-system",
      "BlinkMacSystemFont",
      '"Segoe UI"',
      "Roboto",
      '"Helvetica Neue"',
      "Arial",
      "sans-serif",
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(","),
  },
  palette: {
    baseColor: {
      main: baseColor,
    },
    secendColor: {
      main: secendColor,
    },
    thirdColor: {
      main: thirdColor,
    },
    chip1: {
      main: chip1,
    },
    chip2: {
      main: chip2,
    },
    chip3: {
      main: chip3,
    },
    chip4: {
      main: chip4,
    },
    chip5: {
      main: chip5,
    },
    chipText: {
      main: chipText,
    },
    boxSearch: {
      main: boxSearch,
    },
    white: {
      main: white,
    },
    black: {
      main: black,
    },
    colorButton: {
      main: colorButton,
    },
    textInformation: {
      main: textInformation,
    },
    textMobile: {
      main: textMobile,
    },
  },
  components: {
    MuiTextField: {
      styleOverrides:{
        root:{
          width: "100%",
          marginTop: "32px",
          ".MuiInputLabel-root": {
            fontWeight: "500",
            fontsize: "15px",
            lineheight: "15px",
            display: "flex",
            alignitems: "center",
            color: " #4E5D78",
            transform: "translate(14px, -9px) scale(0.75)",
            background: "white",
            padding: '0 12px',
          },
          ".MuiOutlinedInput-root": {
            borderRadius: "8px",
          },
          ".MuiOutlinedInput-input": {
            padding: "14px",
            fontSize: "14px",
          },
          ".MuiOutlinedInput-notchedOutline": {
            borderColor: "rgb(179 182 239)",
          },
          ".MuiFormHelperText-root":{
            margin: '0',
            fontWeight: 500,
            fontSize: '12px',
            lineHeight: '2.5',
            color: '#F43319',
            paddingLeft: '10px',
            background: 'rgba(244, 51, 25, 0.06)',
            borderRadius: '8px',
            marginTop: '6px',
          }
        }
       
      }
        
      },
    },
  
});

export default theme;
