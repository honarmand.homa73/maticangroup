import { styled } from "@mui/material/styles";
export const MyNavAnchor = styled(({
  as: T = 'Route',
  active, // destructured so it is not passed to anchor in props
  ...props
}) => <T {...props} />)({
  textDecoration: 'blink',
  color: 'blue',
}, ({ active }) => (active && {
  color: 'red',
}));