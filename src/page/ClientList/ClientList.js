import * as React from 'react';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Navbar from '../../Component/Navbar/Navbar';
import Sidbar from '../../Component/Sidbar/Sidbar';
import Clients from '../../Component/Clients/Clients';
export default function  ClientList() {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <Grid container >
        <Grid item xs={2} >
         <Sidbar />
        </Grid>
        <Grid item xs={10} sx={{background: '#F7F8FD'}}>
         <Navbar />
         <Clients />
        </Grid>
      </Grid>
    </Box>
  );
}
