import * as React from "react";
// material
import {Box,Grid} from "@mui/material";

// component
import Header from '../../Component/Navbar/Navbar';
import Navbar from '../../Component/Sidbar/Sidbar';
import NewClient from "../../Component/ProfileClinet/ProfileClient";

export default function ProfileClients() {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <Grid container>
        <Grid item xs={2}>
          <Navbar />
        </Grid>
        <Grid item xs={10}>
          <Header />
          <NewClient />
        </Grid>
      </Grid>
    </Box>
  );
}
