import { styled } from "@mui/system";
import {Box,List,ListItemText,ListItemIcon} from "@mui/material";
import { ReactComponent as ArrowLeft } from "../../assets/Icons/Navbar/ArrowLeft.svg";
export const BoxNavbarTop = styled(Box)({
    height: "248px",
    background: "#3D43CA",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  });
  export const BoxNavbarButton = styled(Box)({
    height: "calc(100vh - 15.5rem)",
    flexDirection: "column",
    background: "#4048D6;",
    display: "flex",
    color: "white",
    '.MuiList-root':{
      margin:'revert'
    }
  });

  
  export const ItemDivider = styled(Box)({
    width: "100%",
    height: "1px",
    height: "1px",
    background: "white",
    opacity: 0.2,
  });
  export const ItemBox = styled(Box)({
    marginTop:' auto',
  });
  export const ListMenu = styled(List)({
   padding:0,
  });

  // itemMenu
  export const Item = styled("li")({
    marginLeft: "15px",
    marginRight: "15px",
    display: "flex",
    alignItems: "center",
    "&:hover": {background: '#242BA8',
      borderRadius: "5px"},
    "&:active": {background: '#242BA8',
      borderRadius: "5px"},
      '.MuiListItemIcon-root':{
        minWidth: 'auto',
      }
  });
  export const ItemIcon = styled(ListItemIcon)({
    paddingLeft: "5px",
    color: "white",
    display: "flex",
  });
  export const ItemText = styled(ListItemText)({
    paddingLeft: "5px",
    color: "white",
    margin: 10,
    '.MuiTypography-root':{
      fontSize:".8rem"
    }
    
  });
  export const IconArrowLeft = styled(ArrowLeft)({
    display: "flex",
    margin: "10px 15px 0px auto",
  });