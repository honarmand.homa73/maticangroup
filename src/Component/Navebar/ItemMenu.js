import { Icon } from "@mui/material";
import * as React from "react";

import {Item,ItemIcon,ItemText} from "./styles"
export default function ItemMenu({ icon, text, size, position }) {
  return (
    <Item sx={{ justifyContent: position }}>
      <ItemIcon>
        <Icon component={icon} fontSize={size} />
      </ItemIcon>
      <ItemText>{text}</ItemText>
    </Item>
  );
}


