import * as React from "react";
import Grid from "@mui/material/Grid";
import SettingsIcon from "@mui/icons-material/Settings";
import KeyboardDoubleArrowLeftOutlinedIcon from "@mui/icons-material/KeyboardDoubleArrowLeftOutlined";
import ItemMenu from "./ItemMenu";
import { ReactComponent as Hom } from "../../assets/Icons/Navbar/Home.svg";
import { ReactComponent as Calendar } from "../../assets/Icons/Navbar/Calendar.svg";
import { ReactComponent as Clients } from "../../assets/Icons/Navbar/Clients.svg";
import { ReactComponent as Staff } from "../../assets/Icons/Navbar/Staff.svg";
import { ReactComponent as Services } from "../../assets/Icons/Navbar/Services.svg";
import { ReactComponent as Offers } from "../../assets/Icons/Navbar/Offers.svg";
import { ReactComponent as Sales } from "../../assets/Icons/Navbar/sales.svg";
import { ReactComponent as Setting } from "../../assets/Icons/Navbar/Setting.svg";
import { ReactComponent as Frame } from "../../assets/Icons/Navbar/Frame.svg";
// styles
import {
  BoxNavbarTop,
  BoxNavbarButton,
  TextNavbar,
  ItemDivider,
  ItemBox,
  ListMenu,
  IconArrowLeft
} from "./styles";

export default function Navbar() {
  const dataNavbar = [
    { id: 1, name: "Hom", icon: Hom },
    { id: 2, name: "Calendar", icon: Calendar },
    { id: 3, name: "Clients", icon: Clients },
    { id: 4, name: "Staff", icon: Staff },
    { id: 5, name: "Services", icon: Services },
    { id: 6, name: "Offers", icon: Offers },
    { id: 7, name: "Sales", icon: Sales },
  ];

  return (
    <Grid container>
      <Grid item xs={12}>
        <BoxNavbarTop>
          <Frame />
        </BoxNavbarTop>
      </Grid>

      <Grid item xs={12}>
        <BoxNavbarButton>
          <ListMenu>
            {dataNavbar.map((items) => (
              <ItemMenu icon={items.icon} text={items.name} size={"small"} />
            ))}
          </ListMenu>
          <ItemBox>
            <ListMenu>
              <ItemDivider />
              <ItemMenu icon={Setting} text={"Setting"} size={"small"}/>
              <ItemDivider />
              <IconArrowLeft size={"large"} />
            </ListMenu>
          </ItemBox>
        </BoxNavbarButton>
      </Grid>
    </Grid>
  );
}
