import * as React from 'react';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import Typography from '@mui/material/Typography';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { AccordionStyle,TypographyTitel } from './styles'
export default function AccordionNewClients({title,Header,children}) {
  return (
    <>
      <AccordionStyle defaultExpanded={true}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <TypographyTitel>
          <Typography  sx={{ flexShrink: 0 }} variant={'h6'}>
            {Header}
          </Typography>
          <Typography sx={{ color: 'text.secondary' }}>{title}</Typography>
          </TypographyTitel>
         
        </AccordionSummary>
        <AccordionDetails>
          {children}
        </AccordionDetails>
      </AccordionStyle>
    </>
  );
}
