import { Accordion, Typography } from '@mui/material';
import { styled } from "@mui/system";
export const AccordionStyle = styled(Accordion)({
   
    boxShadow: '0px 5px 36px rgb(10 31 68 / 13%)',
    borderRadius: '6px',
    padding:"12px",
    '.MuiAccordionDetails-root':{
        borderTop: '1px solid #e5e5e5',
    }
});
export const TypographyTitel = styled(Typography)({
    '.MuiTypography-h6': {
        fontStyle: 'normal',
        fontWeight: '700',
        fontSize: '18px',
        lineHeight: '28px',
    },

    '.MuiTypography-body1': {
        fontStyle: 'normal',
        fontWeight: '500',
        fontSize: '16px',
        lineHeight: '24px',
        display: 'flex',
        alignItems: 'center',
    }

});