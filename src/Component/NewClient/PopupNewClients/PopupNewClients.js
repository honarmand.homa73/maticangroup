import React from "react";
import { Grid,DialogContent, Typography } from "@mui/material";
import { useDispatch } from "react-redux";
import { createClient } from "../../../Redux/clientsSlice";
import { v4 as uuidv4 } from "uuid";
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import { useForm, FormProvider } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import {clientSchema} from'../../../helpers/validations/client';
// styles
import {ButtonSavePoupup, DialogNewClient, DialogTitleNewClient,ButtonClosePoupup,BoxAccordion } from "./styles";
import AccordionNewClients from "../AccordionNewClients/AccordionNewClients";
import FormAdditional from "../FormNewClients/FormAdditional/FormAdditional";
import FormAddress from "../FormNewClients/FormAddress/FormAddress";
import FormPersonal from "../FormNewClients/FormPersonal/FormPersonal";


const schema = clientSchema
export default function PopupNewClients(props) { 
  const { openPopup, setOpenPopup } = props;
  const dispatch = useDispatch();


  // useForm
  const methods = useForm({
    defaultValues: {
      firstName: 'afafas',
      lastName: 'fasfsaf',
      mobile: '9057304753',
      sendNotificationBy: 'fasfaf',
      preferredLang: 'fasfaf',
      gender: 'safasf',
      referralSource: 'fdfdf',
      birthDate: 'afasfsa',
      notes: 'safasf',
      area:'fasfsaf',
      block:'saffasf',
      street:'asfasf',

    },
    resolver: yupResolver(schema),
  });
  const {reset,handleSubmit} = methods;

    // function
    const onSubmitHandler = (data) => {
      dispatch(createClient(data));
      reset();
      setOpenPopup(false)
     
    };

  return (
    <DialogNewClient open={openPopup}>
      <DialogTitleNewClient>
      <ButtonClosePoupup  onClick={()=>{setOpenPopup(false)}}><ArrowBackIcon /></ButtonClosePoupup>
        <Typography>NewClient</Typography>
        
      </DialogTitleNewClient>
      <Grid container sx={{flexWrap:"nowrap"}} >
        <Grid item xs={2}></Grid>
        <Grid item xs={8}>
          <DialogContent>
              <FormProvider {...methods}>
                <form>
                  <BoxAccordion>
                    <AccordionNewClients  Header={'Personal details'} title={"Check & manage name, phone and more about this staff"} ><FormAdditional /></AccordionNewClients>
                  </BoxAccordion>
                 
                  <BoxAccordion>
                    <AccordionNewClients Header={'Personal details'} title={"Check & manage name, phone and more about this staff"} ><FormAddress /></AccordionNewClients>
                  </BoxAccordion>
                  <BoxAccordion>
                    <AccordionNewClients Header={"Personal details"} title={"Check & manage name, phone and more about this staff"}>
                      <FormPersonal />
                    </AccordionNewClients>
                  </BoxAccordion>
                  <ButtonSavePoupup type="button"
                  onClick={handleSubmit(onSubmitHandler)}>save</ButtonSavePoupup>
                  
                </form>
              </FormProvider>  
          </DialogContent>
        </Grid>
        <Grid item xs={2}></Grid>
      </Grid>
    </DialogNewClient>
  );
}
