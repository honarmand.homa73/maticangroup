import { styled } from "@mui/system";
import { Button, Dialog, DialogTitle, Grid,Box } from "@mui/material";

export const DialogNewClient = styled(Dialog)({
  ".MuiPaper-root": {
    minWidth: "100%",
    minHeight: "100%",
  },
});

export const ButtonClosePoupup = styled(Button)({
background:"none",
color:"black"
});

export const DialogTitleNewClient = styled(DialogTitle)({
  display: "flex",
  justifyContent: "space-between",
  minHeight: "80px",
  alignItem: "center",
  boxShadow: "inset 0px -1px 0px rgba(78, 93, 120, 0.25)",
  
  ".MuiTypography-root": {
    fontWeight: "700",
    fontSize: "18px",
    lineHeight: "22px",
    textAlign: "center",
    margin: "0",
    display: "flex",
    alignItems: "center",
    minWidth:'50%'
  },
});
export const ButtonSavePoupup =styled(Button)({
  position:"absolute",
  background: "#4048D6",
  borderRadius: "8px",
  color: "white",
  padding: "0px 24px",
  texTransform: "capitalize",
  fontWeight: "700",
  fontSize: "16px",
  lineHeight: "20px",
  top:' 2%',
    right: '1.7%',
    padding: '11px',
    minWidth: '86px',
    minHeight: '48px',
  "&:hover": { background:'#4048D6'},
});
export const BoxAccordion = styled(Box)({
  margin:'30px 0'
    });
    