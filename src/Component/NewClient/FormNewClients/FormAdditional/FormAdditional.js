import React from "react";
import { useFormContext } from "react-hook-form";
import { Box, Grid, MenuItem, TextField } from "@mui/material";
// styles
import { PhoneInputStyle, TypographyError } from "../styles";


const languages = [
  {
    value: "English",
    label: "English",
  },
  {
    value: "Pershian",
    label: "Pershian",
  },
  {
    value: "Arabi",
    label: "Arabi",
  },
];
export default function FormAdditional(props) {
  // states
  const [language, setLanguage] = React.useState();


  // function
  const {
    register,formState: { errors },watch,setValue} = useFormContext();
  const handleChangeLanguage = (event) => {
    setLanguage(event.target.value);
  };

  return (
    <Box>
      
      <Grid container sx={{flexWrap:"wrap"}}>
        <Grid item xs={6} sx={{ pr: "24px" }}>
          <TextField
            variant="outlined"
            name="firstName"
            type="text"
            label=" First Name "
            onchange={onchange}
            {...register("firstName")}
            error={errors.firstName}
            helperText={errors.firstName&& errors.firstName?.message}
          />
        </Grid>
        <Grid item xs={6}>
          <TextField
            variant="outlined"
            name="lastName"
            type="text"
            label=" Last Name "
            onchange={onchange}
            {...register("lastName")}
            error={errors.lastName}
            helperText={errors.lastName && errors.lastName?.message}
          />
        </Grid>
        <Grid item xs={12} sx={{ mt: "32px" }}>
        <PhoneInputStyle
         inputStyle={{
          width: "100%",
          border: errors.mobile
            ? "1px solid red"
            : "1px solid rgba(64, 72, 214, 0.4)",
          borderRadius: " 8px",
        }}
          country={"ae"}
          placeholder="Mobile number"
          value={watch("mobile")}
          onChange={(phone) => {
            setValue("mobile", phone);
          }}
          name="mobile"
          error={errors.mobile}
          helperText={errors.mobile && errors.mobile?.message}
        />
        
            {(errors.mobile) && <TypographyError>{errors.mobile?.message}</TypographyError> }
        </Grid>
        <Grid item xs={12}>
          <TextField
            variant="outlined"
            name="sendNotificationBy"
            type="text"
            label=" Send notifications by"
            onchange={onchange}
            {...register("sendNotificationBy")}
            error={errors.sendNotificationBy}
            helperText={errors.sendNotificationBy && errors.sendNotificationBy?.message}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            select
            name="preferredLang"
            label="Preferred language"
            value={language}
            onChange={handleChangeLanguage}
            {...register("preferredLang")}
            error={errors.preferredLang}
            helperText={errors.preferredLang && errors.preferredLang?.message}
          >
            {languages.map((option) => (
              <MenuItem key={option.value} value={option.value}>
                {option.label}
              </MenuItem>
            ))}
          </TextField>
         
        </Grid>
      </Grid>
    </Box>
  );
}
