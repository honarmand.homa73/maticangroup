import React from "react";
import { useFormContext } from "react-hook-form";

import { Box, Grid ,MenuItem,TextField} from "@mui/material";


const Genders = [
  {
    value: 'Usa',
    label: 'Usa',
  },

 
];
const Referrals = [
  {
    value: 'exampel1',
    label: 'example1',
  },
  {
    value: 'exampel2',
    label: 'example2',
  },
  {
    value: 'exampel3',
    label: 'example3',
  },

 
];
const Years = [
  {
    value: '1991',
    
  },
  {
    value: '1992',
  
  },
  {
    value: '1993',
    
  },
 
];
const Months = [
  {
    value: 'January',
    
  },
  {
    value: 'Februay',
    
  },
  {
    value: 'March',
    
  },
  {
    value: 'April',
    
  },
  {
    value: 'May',
    
  },
  {
    value: 'June',
   
  },
  {
    value: 'July',
  
  },
  {
    value: 'Augusr',
    
  },
  {
    value: 'September',
  
  },
  {
    value: 'October',
    
  },
  {
    value: 'November',
    
  },
  {
    value: 'December',
   
  },
 
];
const Days = [
  {
    value: 'Saturday',
  },
  {
    value: 'Sunday',
   
  },
  {
    value: 'Monday',
  
  },
  {
    value: 'Tuesday',
  
  },
  {
    value: 'Wednesday',
  
  },
  {
    value: 'Thursday',
  
  },
  {
    value: 'Friday',
  
  },
 
];

export default function FormAddress() {
  const {
    register,
    formState: { errors },
  } = useFormContext();

  const [Gender, setGender] = React.useState();
  const [Referral, setReferral] = React.useState();
  const [Year, setYear] = React.useState();
  const [Month, setMonth] = React.useState();
  const [Day, setDay] = React.useState();

  const handleChangeGender = (event) => {
    setGender(event.target.value);
  };
  const handleChangeReferral = (event) => {
    setReferral(event.target.value);
  };
  const handleChangeYear = (event) => {
    setYear(event.target.value);
  };
  const handleChangeMonth = (event) => {
    setMonth(event.target.value);
  };
  const handleChangeDay = (event) => {
    setDay(event.target.value);
  };
  
  return (
   
    <Box>
      <Grid container sx={{flexWrap:"wrap"}}>
        <Grid item xs={6} sx={{pr:'24px'}}>
            <TextField
            
            select
            name="Gender"
            label="Gender"
            value={Gender}
            onChange={handleChangeGender}
            {...register("Gender")}
            error={errors.Gender}
            helperText={errors.Gender&& errors.Gender?.message}
          >
            {Genders.map((option) => (
            <MenuItem key={option.value} value={option.value}>
              {option.label}
            </MenuItem>
          ))}
            </TextField>
        </Grid>

        <Grid item xs={6} >
            <TextField
           
            select
            name="Referral"
            label="Referral Source"
            onChange={handleChangeReferral}
            {...register("Referral")}
            value={Referral}
            error={errors.Referral}
            helperText={errors.Referral && errors.Referral?.message}
          >
            {Referrals.map((option) => (
            <MenuItem key={option.value} value={option.value}>
              {option.label}
            </MenuItem>
          ))}
            </TextField>

        </Grid>
        
        <Grid item xs={4} sx={{pr:'24px'}}>
            <TextField
         
            select
            name="Year"
            label="Birth Year"
            value={Year}
            onChange={handleChangeYear}
            {...register("Year")}
              error={errors.Year}
            helperText={errors.Year && errors.Year?.message}

          >
            {Years.map((option) => (
            <MenuItem key={option.value} value={option.value}>
              {option.value}
            </MenuItem>
          ))}
            </TextField>

        
        </Grid>
        
        <Grid item xs={4} sx={{pr:'24px'}}>
            <TextField
            select
            name="Month"
            label="Birthday Month"
            value={Month}
            onChange={handleChangeMonth}
            {...register("Month")}
            error={errors.Month}
            helperText={errors.Month && errors.Month?.message}
          >
            {Months.map((option) => (
            <MenuItem key={option.value} value={option.value}>
              {option.value}
            </MenuItem>
          ))}
            </TextField>
        </Grid>

        <Grid item xs={4} >
            <TextField
            select
            name="Day"
            label="Birth Day"
            value={Day}
            onChange={handleChangeDay}
            {...register("Day")}
              error={errors.Day}
            helperText={errors.Day && errors.Day?.message}

          >
            {Days.map((option) => (
            <MenuItem key={option.value} value={option.value}>
              {option.value}
            </MenuItem>
          ))}
            </TextField>
        </Grid>

        <Grid item xs={12}>
          <TextField
            multiline
            placeholder="Enter notes"
            rows={3}
            variant="outlined"
            name="Notes"
            type="text"
            label=" Notes"
            onchange={onchange}
              />

        </Grid>
      
      </Grid>
    </Box>
  );
}