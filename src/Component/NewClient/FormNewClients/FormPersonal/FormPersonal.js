import React from "react";
import { useFormContext } from "react-hook-form";
import { Box, Grid,TextField } from "@mui/material";

export default function FormPersonal() {

  const {
    register,
    formState: { errors },
  } = useFormContext();
  return (
    <Box>
     
    <Grid container sx={{flexWrap:"wrap"}}>
      <Grid item xs={12}>
        <TextField
          variant="outlined"
          name="Area"
          type="text"
          label=" Area"
          {...register("Area")}
          onchange={onchange}
          error={errors.Area}
          helperText={errors.Area&& errors.Area?.message}
            />

        
      </Grid>
      <Grid item xs={12}>
        <TextField
          variant="outlined"
          name="Block"
          type="text"
          label=" Block"
          {...register("Block")}
          onchange={onchange}
          error={errors.Block}
          helperText={errors.Block&& errors.Block?.message}

            />

      </Grid>
      <Grid item xs={12}>
        <TextField
          variant="outlined"
          name="Street"
          type="text"
          label=" Street"
          {...register("Street")}
          onchange={onchange}
          error={errors.Street}
          helperText={errors.Street&& errors.Street?.message}
            />
      </Grid>
      <Grid item xs={12}>
        <TextField
          variant="outlined"
          name="Avenue"
          type="text"
          label=" Avenue"
          {...register("Avenue")}
          onchange={onchange}
          error={errors.Avenue}
          helperText={errors.Avenue&& errors.Avenue?.message}

            />
      </Grid>
      <Grid item xs={12}>
        <TextField
          variant="outlined"
          name="House"
          type="text"
          label=" House number / Building / Apartment"
          {...register("House")}
          onchange={onchange}
          error={errors.House}
          helperText={errors.House && errors.House?.message}
            />
      </Grid>
    </Grid>
  </Box>
  );
}