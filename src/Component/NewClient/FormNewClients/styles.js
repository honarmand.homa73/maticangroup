import { styled } from "@mui/system";
import { Typography} from "@mui/material";
import PhoneInput from 'react-phone-input-2';
import 'react-phone-input-2/lib/material.css';

export const PhoneInputStyle = styled(PhoneInput)({

     '.selected-flag':{
         borderRight:"1px solid #b3b6ef"
     } ,
     '.special-label':{
        fontWeight: '500',
        color:' #4E5D78', 
        
     }
     
   
 
});
export const TypographyError = styled(Typography)({
    fontWeight: '500',
    fontSize: '12px',
    lineHeight: '15px',
    display: 'flex',
    alignItems: 'center',
    color: '#F43319',
    paddingLeft:'10px',
    lineHeight: 2.5,
    background:' rgba(244, 51, 25, 0.06)',
    borderRadius: '8px',
    marginTop:'6px'
});
