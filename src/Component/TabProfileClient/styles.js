import { styled } from "@mui/system";
// material
import {
    Tab,
    Tabs,
    Typography
  } from "@mui/material";
  
export const TabHeader = styled(Tab)({
    textTransform:' revert'
  
});
export const TypographyProductName = styled(Typography)({
   display: '-webkit-box', 
   WebkitBoxOrient: 'vertical',
   WebkitLineClamp: '1',
   overflow: 'hidden',
   textOverflow: 'ellipsis',
   whiteSpace: 'break-spaces',
   lineHeight: '1',
  maxWidth: '100px',
  color: '#0A1F44',
  fontWeight: '500',
fontSize: '14px',
lineHeight: '24px',
  
});
export const TabsHeader = styled(Tabs)(({theme})=>({
  color: theme.palette.thirdColor.main,
  margin: '0 9px',
  '.MuiButtonBase-root':{
    fontWeight: '500',
fontSize: '16px',
lineHeight: '24px',
  }
  
}));