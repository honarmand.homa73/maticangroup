import { styled } from "@mui/system";
import { Box,Typography } from "@mui/material";
import {ReactComponent as Completed} from "../../../../assets/Icons/NewClient/Completed.svg";
export const BoxRow = styled(Box)({
  display:"flex",
  margin:'20px 0',
 
});
export const BoxStarted = styled(Box)({
  
  background:"#53BFB0",
  width:"16px",
  height:"16px",
  borderRadius:"50%",
  display:"flex",
  justifyContent:"center",
  alignItems:"center",
  marginRight:"5px",
  '.MuiSvgIcon-root':{
    fontSize:'12px',
    color:"white"

  },

});
export const BoxColumn1 = styled(Box)({
  display:"flex",
  flexDirection:"column",
  minWidth: '160px',
  ".MuiTypography-h6 ":{
    color: '#0A1F44',
    fontWeight: '500',
    fontSize: '12px',
    lineHeight: '16px',
  },
  '.MuiTypography-caption':{
    fontWeight: '400',
    fontSize: '12px',
    lineHeight: '16px',
    color: '#4E5D78',
    
  },
  '.MuiTypography-body2':{
    display: 'flex',
    alignItems:'center',
    fontWeight: '400',

    lineHeight: '24px',
    letterSpacing: '0.1px',
      marginTop:'9px',

  }
 
});
export const TypographyOnlin = styled(Typography)({
  background:' #4048D6',
    borderRadius: '10px',
    color: 'white',
    fontSize: '12px',
    padding: '2px 10px',
    marginRight:"10px",
    fontWeight: 500,
    display:'flex',
    justifyContent:'center',
    
  ".MuiSvgIcon-root":{
    color: 'white',
    fontSize:'12px',
    paddingRight:'4px'
  }
 
});
export const TypographyThrough = styled(Typography)({
  textDecorationLine: 'line-through',
  color: 'rgba(78, 93, 120, 0.64)',
  fontSize: '12px',
  fontWeight:500
 
});
export const BoxColumn2 = styled(Box)({
  display:"flex",
flexDirection:"column",
width:'100%',
'.MuiTypography-h6':{
  fontWeight: "600",
  fontSize: "14px",
  lineHeight: "20px",
  display: "flex",
  alignItems: "center",
  color: "#0A1F44",
  textAlign: 'justify',
},
'.MuiTypography-body1':{
  fontWeight: '400',
  fontSize: '14px',
  lineHeight: '20px',
  display: 'flex',
  alignItems: 'center',
  color: '#4E5D78',
  textAlign: 'justify',
  paddingBottom: '10px',
}
 
});
export const BoxFind = styled(Box)({
  display:"flex",
  flexDirection:"row",
  alignItems:'center',
});
export const CompletedIcon = styled(Completed)({
marginRight:"5px"
});

export const TypographyBold = styled(Typography)({
  fontWeight: '700',
  fontSize: '14px',
  lineHeight: '24px',
  letterSpacing: '0.1px',
  color: '#4048D6',
});
export const TypographyNormul = styled(Typography)({
  fontWeight: '600',
  fontSize: '14px',
  lineHeight: '24px',
  letterSpacing: '0.1px',
  color: '#000000',
});

export const BoxEpointments = styled(Box)({
 display:"flex",
 justifyContent: 'space-between',
 width: '100%'
});
export const BoxEpointmentsIn = styled(Box)({
  margin: '15px',
  background: '#F5F4F4',
  padding:"8px 18px"
});