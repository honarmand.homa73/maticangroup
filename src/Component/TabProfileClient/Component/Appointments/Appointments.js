import React from 'react'
// material
import {
    Typography,
    Box,
    Grid,
    SvgIcon,
    Divider,
  } from "@mui/material";
  import FiberManualRecordIcon from "@mui/icons-material/FiberManualRecord";
  
  // style
  import {
    BoxRow,
    BoxColumn1,
    BoxStarted,
    BoxColumn2,
    BoxFind,
    TypographyNormul,
    TypographyBold,
    TypographyThrough,
    TypographyOnlin,
    BoxEpointments,
    BoxEpointmentsIn,
    CompletedIcon
  } from "./styles";
  // svg
  import PlayArrowIcon from "@mui/icons-material/PlayArrow";
export default function Appointments() {
  return (
   <>
    <Grid>
          <BoxRow>
            <BoxColumn1>
              <Typography variant="h6">30 Nov</Typography>
              <Typography variant="caption">Monday</Typography>
              <Typography variant="body2">
                <BoxStarted>
                  <PlayArrowIcon />
                </BoxStarted>
                Started
              </Typography>
            </BoxColumn1>
            <BoxColumn2>
              <BoxEpointments>
                <Box>
                  <Typography variant="h6">Beard Package</Typography>
                  <Typography variant="body1">3 services, 1h30min</Typography>
                </Box>
                <BoxFind>
                  <TypographyOnlin variant="h2">
                    <FiberManualRecordIcon /> Online booking
                  </TypographyOnlin>
                  <Box>
                    <TypographyBold variant="h2"> KD 100 </TypographyBold>
                    <TypographyThrough variant="body2"> KD 125 </TypographyThrough>
                  </Box>
                </BoxFind>
              </BoxEpointments>
              <BoxEpointmentsIn>
                <Typography variant="h6">Hair Coloring</Typography>
                <Typography variant="body1">
                  blood of crist, 40m with SoSo Al-Sherrer, 3:45pm
                </Typography>
                <Typography variant="h6">Koutan blood</Typography>
                <Typography variant="body1">
                  blood of crist, 40m with SoSo Al-Sherrer, 3:45pm
                </Typography>
                <Typography variant="h6">Hair Coloring</Typography>
                <Typography variant="body1">
                  blood of crist, 40m with SoSo Al-Sherrer, 3:45pm
                </Typography>
              </BoxEpointmentsIn>
              <BoxEpointments>
                <Box>
                  <Typography variant="h6">Beard Package</Typography>
                  <Typography variant="body1">3 services, 1h30min</Typography>
                </Box>
                <BoxFind>
                
                  <Box>
                    <TypographyNormul variant="h2"> KD 100 </TypographyNormul>
                  </Box>
                </BoxFind>
              </BoxEpointments>
              <BoxEpointments>
                <Box>
                  <Typography variant="h6">Beard Package</Typography>
                  <Typography variant="body1">3 services, 1h30min</Typography>
                </Box>
                <BoxFind>
                
                  <Box>
                    <TypographyNormul variant="h2"> KD 100 </TypographyNormul>
                    
                  </Box>
                </BoxFind>
              </BoxEpointments>
            </BoxColumn2>
          </BoxRow>
        </Grid>
        <Divider />
        <Grid>
          <BoxRow>
            <BoxColumn1>
              <Typography variant="h6">30 Nov</Typography>
              <Typography variant="caption">Monday</Typography>
              <Typography variant="body2">
              <CompletedIcon />
              Completed
              </Typography>
            </BoxColumn1>
            <BoxColumn2>
              <BoxEpointments>
                <Box>
                  <Typography variant="h6">Beard Package</Typography>
                  <Typography variant="body1">3 services, 1h30min</Typography>
                </Box>
                <BoxFind>
                  <TypographyOnlin variant="h2">
                    <FiberManualRecordIcon /> Online booking{" "}
                  </TypographyOnlin>
                  <Box>
                    <TypographyBold variant="h2"> KD 100 </TypographyBold>
                    <TypographyThrough variant="body2"> KD 125 </TypographyThrough>
                  </Box>
                </BoxFind>
              </BoxEpointments>
             
            </BoxColumn2>
          </BoxRow>
        </Grid>
        <Divider />
        <Grid>
          <BoxRow>
            <BoxColumn1>
              <Typography variant="h6">30 Nov</Typography>
              <Typography variant="caption">Monday</Typography>
              <Typography variant="body2">
              <CompletedIcon />
                
              Completed
              </Typography>
            </BoxColumn1>
            <BoxColumn2>
              <BoxEpointments>
                <Box>
                  <Typography variant="h6">Beard Package</Typography>
                  <Typography variant="body1">3 services, 1h30min</Typography>
                </Box>
                <BoxFind>
                  <Box>
                    <TypographyNormul variant="h2"> KD 100 </TypographyNormul>
                  </Box>
                </BoxFind>
              </BoxEpointments>
             
            </BoxColumn2>
          </BoxRow>
        </Grid>
   </>
  )
}
