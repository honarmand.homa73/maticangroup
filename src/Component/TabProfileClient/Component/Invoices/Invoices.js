import React from "react";
import ChipClints from "../../../ChipClints/ChipClints";
// style
import Tables from "../../../Tables/Tables";



export default function Invoices() {
  const invoicesData = {
    headers: [
      {
        name: "Status",
        key: "Status",
      },
      {
        name: "Id",
        key: "Id",
      },
      {
        name: "Date",
        key: "Date",
      },
      {
        name: "Location",
        key: "Location",
      },
      {
        name: "Total",
        key: "Total",
      },
    ],
    data: [
      {
        Status: "Paid",
        Id: "1922",
        Date: "23 November 2021",
        Location: "Bookpeep 1",
        Total: "KD 35",
      },
      {
        Status: "Unpaid",
        Id: "1922",
        Date: "23 November 2021",
        Location: "Bookpeep 1",
        Total: "KD 35",
      },
      {
        Status: "Void",
        Id: "1922",
        Date: "23 November 2021",
        Location: "Bookpeep 1",
        Total: "KD 35",
      },
      {
        Status: "Paid",
        Id: "1922",
        Date: "23 November 2021",
        Location: "Bookpeep 1",
        Total: "KD 35",
      },

      {
        Status: "Exchange",
        Id: "1922",
        Date: "23 November 2021",
        Location: "Bookpeep 1",
        Total: "KD 35",
      },
      {
        Status: "Paid",
        Id: "1922",
        Date: "23 November 2021",
        Location: "Bookpeep 1",
        Total: "KD 35",
      },
      {
        Status: "Exchange",
        Id: "1922",
        Date: "23 November 2021",
        Location: "Bookpeep 1",
        Total: "KD 35",
      },
      
    ],
  };

  const formattedData = () => {
    return data.map((d) => ({
      Status: (
        <ChipClints  type={d.Status}/>
      ),
      Id: d.Id,
      Date: d.Date,
      Location: d.Location,
      Total:d.Total
    }));
  };

  const { headers, data } = invoicesData;
  return (
  
    <Tables headers={headers} data={formattedData()}/>
   
  );
}
