import React from "react";
import Tables from "../../../Tables/Tables";
import {Typography} from "@mui/material";
import {TypographyProductName} from "../../styles"
// dataTable
const productsData = {
  headers: [
    {
      name: "Invocie #",
      key: "Invocie",
    },
    {
      name: "Product name",
      key: "ProductName",
    },
    {
      name: "Date",
      key: "Date",
    },
    {
      name: "Location",
      key: "Location",
    },
    {
      name: "Total",
      key: "Total",
    },
  ],
  data: [
    {
      Invocie: "14458834",
      ProductName: "Oh Bee Hive Oh Bee Hive",
      Date: "23 November 2021",
      Location: "Bookpeep 1",
      Total: "KD 35",
    },
    {
      Invocie: "14458834",
      ProductName: "Oh Bee Hive Oh Bee Hive",
      Date: "23 November 2021",
      Location: "Bookpeep 1",
      Total: "KD 35",
    },
    {
      Invocie: "14458834",
      ProductName: "Oh Bee Hive Oh Bee Hive",
      Date: "23 November 2021",
      Location: "Bookpeep 1",
      Total: "KD 35",
    },
    {
      Invocie: "14458834",
      ProductName: "Oh Bee Hive Oh Bee Hive",
      Date: "23 November 2021",
      Location: "Bookpeep 1",
      Total: "KD 35",
    },
    {
      Invocie: "14458834",
      ProductName: "Oh Bee Hive Oh Bee Hive",
      Date: "23 November 2021",
      Location: "Bookpeep 1",
      Total: "KD 35",
    },

  ],
};

export default function Products() {
  const formattedData = () => {
    return data.map((d) => ({
      Invocie: (
        <Typography sx={{color: '#4048D6'}} component="th" scope="row">{d.Invocie}</Typography>
      ),
      ProductName:(
        <TypographyProductName>{d.ProductName}</TypographyProductName>
      ),
      Date: d.Date,
      Location: d.Location,
      Total:d.Total
    }));
  };
  const{headers, data}=productsData
  return (
    
  <Tables headers={headers} data={formattedData()}/>
   
  );
}
