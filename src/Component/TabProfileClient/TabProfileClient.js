import * as React from "react";
import PropTypes from "prop-types";
// material
import {
  Tabs,
  Typography,
  Box,
} from "@mui/material";

// style
import {
  TabHeader,TabsHeader
} from "./styles";
// svg
import Appointments from "./Component/Appointments/Appointments";
import Invoices from "./Component/Invoices/Invoices";
import Products from "./Component/Products/Products";


// function
function TabPanel(props) {
  const { children, value, index, ...other } = props;
  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

export default function TabProfileClient() {
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <Box sx={{ width: "100%", my: 3 }}>
      <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
        <TabsHeader
          value={value}
          onChange={handleChange}
          aria-label="basic tabs example">
          <TabHeader sx={{}} label="Appointments" {...a11yProps(0)} />
          <TabHeader  sx={{textTransform:' revert'}} label="Invoices" {...a11yProps(1)} />
          <TabHeader  sx={{textTransform:' revert'}} label="Products" {...a11yProps(2)} />
        </TabsHeader>
      </Box>
      <TabPanel value={value} index={0}>
        <Appointments />
      </TabPanel>
      <TabPanel value={value} index={1}>
       <Invoices />
      </TabPanel>
      <TabPanel value={value} index={2}>
        <Products />
      </TabPanel>
    </Box>
  );
}
