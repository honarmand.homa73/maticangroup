import * as React from "react";
import {Toolbar,IconButton} from "@mui/material";
// svg
import {ReactComponent as Search} from "../../assets/Icons/Header/Search.svg";
import {ReactComponent as Notifications} from "../../assets/Icons/Header/Notifications.svg";
import avatar  from '../../assets/Icons/Header/avatar.svg';

// styles
import {
  BoxflexGrow,
  TypographyClients,
  AppBarStyle,
  IconButtonStyle,
  AvatarStyle,
  BoxIcons,
  SvgIconSearch
} from "./styles";

export default function Navbar() {
  return (
    
      <AppBarStyle position="static">
        <Toolbar>
          <TypographyClients >
            Clients
          </TypographyClients>
          <BoxflexGrow />
          <BoxIcons>
            <IconButtonStyle >
            <SvgIconSearch component={Search} size="small"/>
            </IconButtonStyle>
            <IconButtonStyle>
              <Notifications size="small" />
            </IconButtonStyle>
            <IconButton>
              <AvatarStyle alt="Remy Sharp" src={avatar} />
            </IconButton>
          </BoxIcons>
        </Toolbar>
      </AppBarStyle>
    
  );
}