import { styled } from "@mui/material/styles";
import {
  AppBar,
  Box,
  Typography,
  IconButton,
  Avatar,
  SvgIcon,
} from "@mui/material";
export const BoxflexGrow = styled(Box)({
  flexGrow: "1",
});
export const TypographyClients = styled(Typography)(({theme})=>({
  fontStyle: "normal",
  fontWeight: "700",
  fontSize: "24px",
  lineHeight: " 30px",
  display: "flex",
  alignItems: "center",
  color: theme.palette.black.main,
}));
export const AppBarStyle = styled(AppBar)(({theme})=>({
  display: "flex",
  background: theme.palette.white.main,
  boxShadow: "inset 0px -1px 0px rgba(78, 93, 120, 0.25)",
  color: theme.palette.chipText.main,
}));
export const IconButtonStyle = styled(IconButton)(({theme})=>({
  color: theme.palette.colorButton.main,
  padding:'5px'
}));
export const AvatarStyle = styled(Avatar)({
  width: "25px",
  height: "25px",
});
export const BoxIcons = styled(Box)({
  display: "flex",
  alignItems: "center",
});
export const SvgIconSearch = styled(SvgIcon)({
  marginTop: "3px",
});
