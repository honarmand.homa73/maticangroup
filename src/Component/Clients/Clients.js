import * as React from "react";
import SearchIcon from "@mui/icons-material/Search";


// component
import TableClient from "../TableClients/TableClients";
import PopupNewClients from "../NewClient/PopupNewClients/PopupNewClients";
// styles
import {
  BoxTop,
  BoxSearch,
  ButtonClients,
  IconButtonStyle,
  InputBaseStyle,
} from "./styles";

export default function Clients() {

  const [openPopup, setOpenPopup] = React.useState(false);

  return (
    <>
      <BoxTop>
        <BoxSearch>
          <IconButtonStyle type="submit">
            <SearchIcon fontSize="small" />
          </IconButtonStyle>
          <InputBaseStyle placeholder="Search " inputProps={{ "aria-label": "search" }} />
        </BoxSearch>
        <ButtonClients variant="contained" onClick={() => { setOpenPopup(true); }} >
          New Client
        </ButtonClients>
      </BoxTop>
      <TableClient />
      <PopupNewClients openPopup={openPopup} setOpenPopup={setOpenPopup} />
    </>
  );
}
