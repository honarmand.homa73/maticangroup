import { styled } from "@mui/system";
import {Box, Button,IconButton,InputBase} from "@mui/material";
import { Link } from "react-router-dom";
export const BoxTop = styled(Box)({
    display: "flex",
    justifyContent: "space-between",
    marginTop: "10px",
    marginLeft:'35px',
    marginRight:"35px",
  });
 export const BoxSearch = styled(Box)(({theme})=>({
    background: theme.palette.boxSearch.main,
    borderRadius: "8px",
    display: "flex",
    alignItems: "center",
    minWidth:'30%'
  }));
 export const IconButtonStyle = styled(IconButton)({
  p: "10px"
 });
 export const InputBaseStyle = styled(InputBase)({
  ml: 1, flex: 1
 });
 export const ButtonClients = styled(Button)(({theme})=>({
  textTransform: "unset",
    fontSize: "0.875rem",
    lineHeight: "1.75",
    minWidth: "64px",
    padding: "12px 30px",
    borderRadius: "8px",
    color: theme.palette.white.main,
    backgroundColor: theme.palette.thirdColor.main,
    boxShadow:'none'
  }));
 export const LinkButton = styled(Link)({
  textDecoration:'none'
  });
