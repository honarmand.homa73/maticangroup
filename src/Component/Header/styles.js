import {styled} from '@mui/material/styles';
import {AppBar, Box,Typography,IconButton,Avatar,SvgIcon} from '@mui/material';
export const BoxflexGrow = styled(Box)({
    flexGrow: '1'
  });
  export const TypographyClients = styled(Typography)({
    fontStyle: 'normal',
  fontWeight: '700',
  fontSize:'24px',
  lineHeight:' 30px',
  display: 'flex',
  alignItems: 'center',
  color:'#00000'
  });
  export const AppBarStyle = styled(AppBar)({
    display: 'flex',
  background: '#FFFFFF',
  boxShadow: 'inset 0px -1px 0px rgba(78, 93, 120, 0.25)',
    color:'#0A1F44',
  });
  export const IconButtonStyle = styled(IconButton)({
    color: '#8E98A9',
  });
  export const AvatarStyle = styled(Avatar)({
    width:"25px",
    height:"25px",

  });
  export const BoxIcons = styled(Box)({
   display:"flex",
   alignItems:"center"

  });
  export const SvgIconSearch = styled(SvgIcon)({
    marginTop:"3px",

  });