import * as React from "react";
// material
import { Icon } from "@mui/material";
// style
import {Item,ItemIcon,ItemText} from "./styles"


export default function ItemMenu({ icon, text, size, position }) {
 
  return (
    <Item sx={{ justifyContent: position }}>
      <ItemIcon >
        <Icon component={icon} fontSize={size}/>
      </ItemIcon>
      <ItemText>{text}</ItemText>
    </Item>
  );
}


