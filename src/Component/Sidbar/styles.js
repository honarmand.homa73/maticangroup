import { styled } from "@mui/system";
import { Box, List, ListItemText, ListItemIcon,Grid } from "@mui/material";
import { ReactComponent as ArrowLeft } from "../../assets/Icons/Navbar/ArrowLeft.svg";
export const GridRow = styled(Grid)({
  position:"fixed", 
  maxWidth: "16.666667%;"
});
export const BoxNavbarTop = styled(Box)(({theme})=>({
  height: "14.5rem",
  background:theme.palette.secendColor.main,
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
}));

export const BoxNavbarButton = styled(Box)(({theme})=>({
  height: "calc(100vh - 14.5rem)",
  flexDirection: "column",
  background: theme.palette.thirdColor.main,
  display: "flex",
  position:'fix',
  color: "white",
  ".MuiList-root": {
    margin: "revert",
  },
}));

export const ItemDivider = styled(Box)({
  width: "100%",
  height: "1px",
  height: "1px",
  background: "white",
  opacity: 0.2,
});
export const ItemBox = styled(Box)({
  marginTop: " auto",
});
export const ListMenu = styled(List)({
  padding: 0,
});

// itemMenu
export const Item = styled("li")( ({theme})=>(  {
  marginLeft: "12px",
  marginRight: "12px",
  display: "flex",
  alignItems: "center",
  cursor:"pointer",
  "&:hover": { background: theme.palette.baseColor.main, borderRadius: "5px" },
  "&:active": { background: theme.palette.baseColor.main, borderRadius: "5px" },
  ".MuiListItemIcon-root": {
    minWidth: "auto",
  },
}));
export const ItemIcon = styled(ListItemIcon)({
  paddingLeft: "5px",
  color: "white",
  display: "flex",
});
export const ItemText = styled(ListItemText)({
  paddingLeft: "5px",
  color: "white",
  margin: 10,
  ".MuiTypography-root": {
    fontSize: "1rem",
  },
});
export const IconArrowLeft = styled(ArrowLeft)({
  display: "flex",
  margin: "10px 15px 0px auto",
});
