import * as React from "react";
// material
import Grid from "@mui/material/Grid";
// component
import ItemMenu from "./ItemMenu";
// svg
import { ReactComponent as Hom } from "../../assets/Icons/Navbar/Home.svg";
import { ReactComponent as Calendar } from "../../assets/Icons/Navbar/Calendar.svg";
import { ReactComponent as Clients } from "../../assets/Icons/Navbar/Clients.svg";
import { ReactComponent as Staff } from "../../assets/Icons/Navbar/Staff.svg";
import { ReactComponent as Services } from "../../assets/Icons/Navbar/Services.svg";
import { ReactComponent as Offers } from "../../assets/Icons/Navbar/Offers.svg";
import { ReactComponent as Sales } from "../../assets/Icons/Navbar/sales.svg";
import { ReactComponent as Setting } from "../../assets/Icons/Navbar/Setting.svg";
import { ReactComponent as Frame } from "../../assets/Icons/Navbar/Frame.svg";
// styles
import {
  BoxNavbarTop,
  BoxNavbarButton,
  ItemDivider,
  ItemBox,
  ListMenu,
  IconArrowLeft,
  GridRow
} from "./styles";

export default function Sidbar() {
  const dataNavbar = [
    { id: 1, name: "Home", icon: Hom },
    { id: 2, name: "Calendar", icon: Calendar },
    { id: 3, name: "Clients", icon: Clients },
    { id: 4, name: "Staff", icon: Staff },
    { id: 5, name: "Services", icon: Services },
    { id: 6, name: "Offers", icon: Offers },
    { id: 7, name: "Sales", icon: Sales },
  ];

  return (
    <GridRow container>
      <Grid item xs={12}>
        <BoxNavbarTop>
          <Frame />
        </BoxNavbarTop>
      </Grid>
      <Grid item xs={12}>
        <BoxNavbarButton>
          <ListMenu>
            {dataNavbar.map((items) => (
              <ItemMenu icon={items.icon} text={items.name}  />
            ))}
          </ListMenu>
          <ItemBox>
            <ListMenu>
              <ItemDivider />
              <ItemMenu icon={Setting} text={"Setting"}  />
              <ItemDivider />
              <IconArrowLeft size={"large"} />
            </ListMenu>
          </ItemBox>
        </BoxNavbarButton>
      </Grid>
      
    </GridRow>
  );
}
