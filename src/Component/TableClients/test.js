import * as React from "react";
import {TableRow,Chip,TablePagination,TableHead,TableCell,TableBody,Table,Box} from "@mui/material";

import {
  TableContainerStyle,
  TableCellHeader,
  TableCellName,
  TableCellMobile,
  TableCellEmail,
  TableCellStatus,
} from "./styles";
function createData(name, mobile, email, status) {
  return { name, mobile, email, status };
}

const rows = [
  createData('Beockly simmons', '(159) 555 012', "bill.sanders@exampel.com", 'vip'),
  createData('Beockly simmons', '(159) 555 012', "bill.sanders@exampel.com", "Regular"),
  createData('Beockly simmons', '(159) 555 012', "bill.sanders@exampel.com", "New"),
  createData('Beockly simmons', '(159) 555 012', "bill.sanders@exampel.com", 'vip'),
  createData('Beockly simmons', '(159) 555 012', "bill.sanders@exampel.com", 'vip'),
  createData('Beockly simmons', '(159) 555 012', "bill.sanders@exampel.com", 'vip'),
  createData('Beockly simmons', '(159) 555 012', "bill.sanders@exampel.com", 'vip'),
  createData('Beockly simmons', '(159) 555 012', "bill.sanders@exampel.com", 'vip'),
  createData('Beockly simmons', '(159) 555 012', "bill.sanders@exampel.com", 'vip'),
  createData('Beockly simmons', '(159) 555 012', "bill.sanders@exampel.com", 'vip'),
  createData('Beockly simmons', '(159) 555 012', "bill.sanders@exampel.com", 'vip'),
  createData('Beockly simmons', '(159) 555 012', "bill.sanders@exampel.com", 'vip'),
  createData('Beockly simmons', '(159) 555 012', "bill.sanders@exampel.com", 'vip'),
];

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) {
      return order;
    }
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}





export default function Test() {
  const [order, setOrder] = React.useState("asc");
  const [orderBy, setOrderBy] = React.useState("calories");
  const [selected, setSelected] = React.useState([]);
  const [page, setPage] = React.useState(0);
  const [dense, setDense] = React.useState(false);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const isSelected = (name) => selected.indexOf(name) !== -1;

  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rows.length) : 0;

  return (
    <Box sx={{ width: "100%" }}>
      <TableContainerStyle>
        <Table sx={{ minWidth: 640 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCellHeader align="left">Name</TableCellHeader>
              <TableCellHeader align="left">Mobile</TableCellHeader>
              <TableCellHeader align="left">Email</TableCellHeader>
              <TableCellHeader align="left">Status</TableCellHeader>
            </TableRow>
          </TableHead>
          <TableBody>
            {stableSort(rows, getComparator(order, orderBy))
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((row, index) => {
                const isItemSelected = isSelected(row.name);
                const labelId = `enhanced-table-checkbox-${index}`;

                return (
                  <TableRow
                  key={row.name}
                  sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                >
                  <TableCellName  align="left">{row.name}</TableCellName>
                  <TableCellMobile align="left">{row.mobile}</TableCellMobile>
                  <TableCellEmail align="left">{row.email}</TableCellEmail>
                  <TableCellStatus align="left"><Chip label={row.status} /></TableCellStatus>
                 
                </TableRow>
                );
              })}
            {emptyRows > 0 && (
              <TableRow
                style={{
                  height: (dense ? 33 : 53) * emptyRows,
                }}
              >
                <TableCell colSpan={6} />
              </TableRow>
            )}
          </TableBody>
        </Table>
      </TableContainerStyle>
      <TablePagination
        rowsPerPageOptions={[5, 10, 25]}
        component="div"
        count={rows.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </Box>
  );
}
