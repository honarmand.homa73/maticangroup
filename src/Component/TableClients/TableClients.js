import * as React from "react";
import { useSelector } from 'react-redux';
import {Box} from "@mui/material";
import Tables from "../Tables/Tables";
import { useNavigate } from "react-router-dom";

// styles
import {
  TableCellName,
  TableCellMobile,
  TableCellEmail,
} from "./styles";
import ChipClints from "../ChipClints/ChipClints";


// dataTable
const clientsData = {
  headers: [
    {
      name: "Name",
      key: "Name",
    },
    {
      name: "Mobile",
      key: "Mobile",
    },
    {
      name: "Email",
      key: "Email",
    },
    {
      name: "Status",
      key: "Status",
    },
  ],
  data: [
    {
      Name: "Beockly simmons",
      Mobile: "(159) 555 012",
      Email: "bill.sanders@exampel.com",
      Status: "VIP",
    },
    {
      Name: "Beockly simmons",
      Mobile: "(159) 555 012",
      Email: "bill.sanders@exampel.com",
      Status: "Regular",
    },
    {
      Name: "Beockly simmons",
      Mobile: "(159) 555 012",
      Email: "bill.sanders@exampel.com",
      Status: "New",
    },
    {
      Name: "Beockly simmons",
      Mobile: "(159) 555 012",
      Email: "bill.sanders@exampel.com",
      Status: "VIP",
    },
    {
      Name: "Beockly simmons",
      Mobile: "(159) 555 012",
      Email: "bill.sanders@exampel.com",
      Status: "VIP",
    },
    

  ],
};

export default function Test() {
const navigate = useNavigate();
  const tableData = useSelector((state) => state.clients);
  const { ids, entities } = tableData;
// function
function onClickRow(id){
  navigate(`\${id}`)
  }
    const formattedData = () => {
    return ids.map((i) => ({
     
      Name: (
        <TableCellName align="left"> {entities[i].firstName} {entities[i].lastName}</TableCellName>
      ),
      Mobile: (
        <TableCellMobile align="left"> {entities[i].mobile}</TableCellMobile>
      ),
      Email:  (
        <TableCellEmail align="left">{entities[i].email}</TableCellEmail>
      ),
      Status:  (<ChipClints type={entities[i].status} />),
     
    })
    
    );
  };
  
const{headers,data}=clientsData
  return (
    <Box sx={{mx:"54px"}}>
      <Tables headers={headers} data={formattedData()} Link={()=>onClickRow(ids)} />
    </Box>
  );
}
