import { styled } from "@mui/material/styles";
import { TableContainer,TableCell,TablePagination,TableRow,Typography } from "@mui/material";
import { red } from "@mui/material/colors";

export const TableContainerStyle = styled(TableContainer)({
  margin: "2.2rem",
  width: "auto",
 
});
export const TableCellHeader = styled(TableCell)({
    fontStyle: "normal",
    fontWeight: "700",
    fontSize: "16px",
    lineHeight: "20px",
  
});
export const TableCellName = styled(Typography)(({theme})=>({
    fontStyle: 'normal',
    fontWeight: '500',
    fontSize: '16px',
    lineHeight: '24px',
    letterSpacing: '0.1px',
    color: theme.palette.chipText.main,
    
}));
export const TableCellMobile = styled(Typography)(({theme})=>({
    fontStyle: 'normal',
    fontWeight: '500',
    fontSize: '16px',
    lineHeight: '24px',
    letterSpacing: '0.1px',
    color:theme.palette.textMobile.main,
  
}));
export const TableCellEmail = styled(Typography)(({theme})=>({
    fontStyle: 'normal',
    fontWeight: '500',
    fontSize: '16px',
    lineHeight: '24px',
    letterSpacing: '0.1px',
    color: theme.palette.thirdColor.main,
  
}));
export const TableRowInformation = styled(TableRow)(({theme})=>({
  "&:last-child td, &:last-child th": { border: 0 },
   textDecoration: "none",
  
}));

export const TablePaginationStyle = styled(TablePagination)({
  display:'flex',
  width: '100%',
".MuiToolbar-root":{
  display:'flex',
  width: '100%',
},
 ".MuiTablePagination-selectLabel":{
  order: 1,
  
 },
 ".MuiInputBase-root":{
  order: 2
 },


});
