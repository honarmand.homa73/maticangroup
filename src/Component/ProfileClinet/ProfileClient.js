import * as React from "react";
// material
import {Box,Grid,Paper} from "@mui/material";

// component
import TabProfileClient from "../TabProfileClient/TabProfileClient";
import SidbarLeft from "./Component/SidbarLeft/SidbarLeft";
import TopMain from "./Component/TopMain/TopMain";
import PopupNewClients from "../NewClient/PopupNewClients/PopupNewClients";

export default function NewClient() {
  const [openPopup, setOpenPopup] = React.useState(false);
  return (
    <Box sx={{ flexGrow: 1, mt: 3 }}>
    <Grid container>
      <Grid item xs={4}>
       <SidbarLeft setOpenPopup={setOpenPopup}/>
      </Grid>
      <Grid item xs={8} sx={{paddingRight:'24px'}}>
        <TopMain />
        <Paper elevation={3} square>
          <TabProfileClient />
        </Paper>

      </Grid>
    </Grid>
    
    <PopupNewClients openPopup={openPopup} setOpenPopup={setOpenPopup} />
  </Box>
  );
}
