import React from "react";
// material
import {Box,Typography} from "@mui/material";
// style
import {BoxStatistuon2,BoxStatistuon,PaperBoxTop} from "./styles";

export default function TopMain() {
  return (
    <>
      <PaperBoxTop  square>
        <BoxStatistuon>
          <Box>
            <Typography variant="h6">KD 0</Typography>
            <Typography variant="body1">Total Sales</Typography>
          </Box>
          <Box>
            <Typography variant="h6">KD 0</Typography>
            <Typography variant="body1">Outstanding</Typography>
          </Box>
        </BoxStatistuon>
        <BoxStatistuon2>
          <Box>
            <Typography variant="body1">No Shows</Typography>
            <Typography variant="h6">1</Typography>
          </Box>
          <Box>
            <Typography variant="body1">Cancelled</Typography>
            <Typography variant="h6">2</Typography>
          </Box>
          <Box>
            <Typography variant="body1">Completed</Typography>
            <Typography variant="h6">6</Typography>
          </Box>
          <Box>
            <Typography variant="body1">All Bookings</Typography>
            <Typography variant="h6">9</Typography>
          </Box>
        </BoxStatistuon2>
      </PaperBoxTop>
    </>
  );
}
