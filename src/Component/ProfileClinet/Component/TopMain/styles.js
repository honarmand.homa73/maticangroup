import { styled } from "@mui/material/styles";
import {Box, Paper} from "@mui/material";

export const BoxStatistuon = styled(Box)(({theme})=>({
 display:'flex',
 '.MuiTypography-h6':{
  fontWeight: '500',
  fontSize: '18px',
  lineHeight: '28px',
 },
  ".MuiBox-root": {
    flex: '1',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    boxShadow: 'inset -1px -1px 1px rgb(0 0 0 / 10%)',
    padding: '21px',
  },
  '.MuiTypography-body1':{
    color: theme.palette.textInformation.main,
    fontSize: '16px',
    fontWeight:"400",
    lineHeight:'24px'
  }
 
}));
export const PaperBoxTop = styled(Paper)({
  background: '#FFFFFF',
  boxShadow: '0px 8px 34px rgba(10, 31, 68, 0.08)',
  borderRadius: '4px',
});
export const BoxStatistuon2 = styled(Box)(({theme})=>({
  display: 'flex',
  justifyContent: 'space-around',
  padding: '18px',
  ".MuiBox-root": {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding:' 24px 56px',
    borderRadius: '4px',
    background: '#F7F8FD',
    margin: '6px',

  },
  '.MuiTypography-body1':{
    color:theme.palette.textInformation.main,
    fontSize: '12px',
    lineHeight: '12px',
    fontWeight:'500'
  },
 '.MuiTypography-h6':{
  fontSize: '20px',
  lineHeight: '28px',
  fontWeight: '500',
 }
}));
