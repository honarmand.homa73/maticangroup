import React from 'react'
import EditOutlinedIcon from "@mui/icons-material/EditOutlined";
// material
import {List,Typography,Divider,Container,FormControlLabel,Button,Paper} from "@mui/material";
// style
import {IOSSwitch,BoxEditProfile,TypographyTitr,BoxButton,BoxIOSSwitch,BoxInformation,TypographyTitrAddres,BoxSidbar} from "./styles";
import ChipClints from '../../../ChipClints/ChipClints';
import { Block } from '@mui/icons-material';


export default function SidbarLeft({setOpenPopup}) {
  const [vip, setVip] = React.useState( false);
  const [block, setBlock] = React.useState(false);
  const handleChangeVip = (event) => {
    setVip(event.target.checked );
   
  };
  const handleChangeBlock = (event) => {
    setBlock(event.target.checked );
   
  };
  return (
    <>
     <Paper elevation={1} sx={{ mx: 3 }} square>
          <List>
            <BoxEditProfile>
              <EditOutlinedIcon fontSize='small' />
              <Typography variant="body1" component="p" onClick={() => { setOpenPopup(true); }}>
                Edit
              </Typography>
            </BoxEditProfile>
            <TypographyTitr variant="h5" component="p" gutterBottom>
              Hessa Al Hamlan
            </TypographyTitr>
            <BoxSidbar>
              {vip? <ChipClints type={'VIP'} />:''}
              {block? <ChipClints type={'Blocked'} />:''}
              
              
            </BoxSidbar>
            <BoxButton>
              <Button variant="contained" disableElevation>
                New appointment
              </Button>
            </BoxButton>
            <BoxIOSSwitch>
              <FormControlLabel
               onChange={handleChangeBlock} checked={block}
               name="checkedBlock"
                label="Block"
                control={<IOSSwitch sx={{ m: 1 }} />}
              />
              <FormControlLabel
               onChange={handleChangeVip} checked={vip}
               name="checkedVip"
                label="VIP"
                control={<IOSSwitch sx={{ m: 1 }}  />}
              />
            </BoxIOSSwitch>
          </List>
          <Divider />
          <List>
            <Container>
              <BoxInformation>
                <Typography variant="body1">Mobile</Typography>
                <Typography variant="h6">+965 60 00 12 87</Typography>
              </BoxInformation>
              <BoxInformation>
                <Typography variant="body1">Email</Typography>
                <Typography variant="h6">
                  h.alhamlan@gmail.com
                </Typography>
              </BoxInformation>
              <BoxInformation>
                <Typography variant="body1">Date of Birth</Typography>
                <Typography variant="h6">11 July 1985</Typography>
              </BoxInformation>
              <BoxInformation>
                <Typography variant="body1">Gender</Typography>
                <Typography variant="h6">Female</Typography>
              </BoxInformation>
              <BoxInformation>
                <Typography variant="body1">Client Notes</Typography>
                <Typography variant="h6">
                  Lorem ipsum dolor sit amet, consectetur adipiscing
                  elit
                </Typography>
              </BoxInformation>
            </Container>
          </List>
          <Divider />
          <List>
            <Container>
            <TypographyTitrAddres variant="h6">Address</TypographyTitrAddres>
              <BoxInformation>
                <Typography variant="body1">Area</Typography>
                <Typography variant="h6">Sebastiana Chosta</Typography>
              </BoxInformation>
              <BoxInformation>
                <Typography variant="body1">Block</Typography>
                <Typography variant="h6">Opanoun 12</Typography>
              </BoxInformation>
              <BoxInformation>
                <Typography variant="body1">Street</Typography>
                <Typography variant="h6">Street</Typography>
              </BoxInformation>
              <BoxInformation>
                <Typography variant="body1">Avenue</Typography>
                <Typography variant="h6">
                  Airport Street (SY-NC-TC)
                </Typography>
              </BoxInformation>
              <BoxInformation>
                <Typography variant="body1">
                  House/Building/Apartment #
                </Typography>
                <Typography variant="h6">12</Typography>
              </BoxInformation>
            </Container>
          </List>
        </Paper></>
  )
}
