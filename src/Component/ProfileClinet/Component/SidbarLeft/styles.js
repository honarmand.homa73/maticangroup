import { styled } from "@mui/material/styles";
import { Switch, Box, Typography, Button } from "@mui/material";


export const IOSSwitch = styled((props) => (
  <Switch focusVisibleClassName=".Mui-focusVisible" disableRipple {...props} />
))(({ theme }) => ({
  width: 42,
  height: 26,
  padding: 0,
 
  "& .MuiSwitch-switchBase": {
    padding: 0,
    margin: 2,
    transitionDuration: "300ms",
    "&.Mui-checked": {
      transform: "translateX(16px)",
      color: theme.palette.white.main,
      "& + .MuiSwitch-track": {
        backgroundColor: theme.palette.mode === "dark" ? "#2ECA45" : "#65C466",
        opacity: 1,
        border: 0,
      },
      "&.Mui-disabled + .MuiSwitch-track": {
        opacity: 0.5,
      },
    },
    "&.Mui-focusVisible .MuiSwitch-thumb": {
      color: "#33cf4d",
      border: "6px solid #fff",
    },
    "&.Mui-disabled .MuiSwitch-thumb": {
      color:
        theme.palette.mode === "light"
          ? theme.palette.grey[100]
          : theme.palette.grey[600],
    },
    "&.Mui-disabled + .MuiSwitch-track": {
      opacity: theme.palette.mode === "light" ? 0.7 : 0.3,
    },
  },
  "& .MuiSwitch-thumb": {
    boxSizing: "border-box",
    width: 22,
    height: 22,
  },
  "& .MuiSwitch-track": {
    borderRadius: 26 / 2,
    backgroundColor: theme.palette.mode === "light" ? "#E9E9EA" : "#39393D",
    opacity: 1,
    transition: theme.transitions.create(["background-color"], {
      duration: 500,
    }),
  },
}));

export const BoxEditProfile = styled(Box)(({ theme }) => ({
  display: "flex",
  justifyContent: "flex-end",
  color: theme.palette.thirdColor.main,
  ".MuiTypography-root": {
    fontWeight: "700",
    fontSize: "16px",
    padding:"0 10px",
    lineHeight: '20px',
  },
}));
export const TypographyTitr = styled(Typography)({
  fontWeight: "700",
  fontSize: "32px",
  lineHeight: "38px",
  display: "flex",
  alignItems: "center",
  margin: "20px 0",
  justifyContent: "center",
});
export const TypographyTitrAddres = styled(Typography)({
  fontStyle: "normal",
  fontWeight: "700",
  fontSize: "16px",
  lineHeight: "16px",
  margin:'15px 0'
});
export const BoxButton = styled(Button)(({ theme }) => ({
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  marginBottom: "20px",
  margin:"auto",
  ".MuiButton-root": {
    background: theme.palette.thirdColor.main,
    borderRadius: "8px",
    padding:"14px 90px",
    textTransform: 'revert',
    fontSize:'16px',
    fontWeight:700,
    lineHeight:"20px"
  },
}));
export const BoxIOSSwitch = styled(Box)({
  display: "flex",
  alignItems: "center",
  justifyContent: "space-around",
  margin: "20px 40px",
  ".MuiFormControlLabel-root": {
    direction: "rtl",
    margin:0
  },
  ".MuiTypography-root": {
    fontSize: "14px",
  },
});
export const BoxInformation = styled(Box)(({ theme }) => ({
  marginBottom: "16px",
  ".MuiTypography-body1": {
    color: theme.palette.textInformation.main,
    fontWeight: "400",
    fontSize: "16px",
    lineHeight: "24px",
  },
  '.MuiTypography-h6':{
    fontSize: '16px',
    lineHeight: '24px',
    fontWeight:'500',
    color: 'black',
  }
}));
export const BoxStatistuon = styled(Box)(({ theme }) => ({
 display:'flex',
  ".MuiBox-root": {
    flex: '1',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    boxShadow: 'inset -1px -1px 1px rgb(0 0 0 / 10%)',
    padding: '15px',
  },
  '.MuiTypography-body1':{
    color: theme.palette.textInformation.main,
    fontSize: '14px',
  }
 
}));
export const BoxStatistuon2 = styled(Box)(({ theme }) => ({
  display: 'flex',
  justifyContent: 'space-around',
  padding: '30px',
  ".MuiBox-root": {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  '.MuiTypography-body1':{
    color:theme.palette.textInformation.main,
    fontSize: '12px',
    lineHeight: '12px',
  },
 '. MuiTypography-h6':{
  fontSize: '20px',
  lineHeight: '28px',
  fontWeight: '600',
 }
}));
export const BoxSidbar=styled(Box)({
  display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around',
    margin: '0 20% 6%',
})
