import { styled } from "@mui/material/styles";
import { Chip } from "@mui/material";



export const StatusVip = styled(Chip)(({theme})=>({
        borderRadius: '8px',
        padding:'2px 0',
        fontSize: '.8rem',
        width:'88px',
        background: theme.palette.chip1.main,
        color: theme.palette.chipText.main,
   
}));
export const StatusRegular = styled(Chip)(({theme})=>({
        borderRadius: '8px',
        padding:'2px 0',
        fontSize: '.8rem',
        width:'88px',
        background: theme.palette.chip2.main,
        color: theme.palette.chipText.main,
   
}));
export const StatusNew = styled(Chip)(({theme})=>({
        borderRadius: '8px',
        padding:'2px 0',
        fontSize: '.8rem',
        minWidth:'88px',
        background: theme.palette.chip3.main,
        color: theme.palette.chipText.main,
   
}));
export const StatusUpadid = styled(Chip)(({theme})=>({
        borderRadius: '8px',
        padding:'2px 0',
        fontSize: '.8rem',
        width:'88px',
        background:theme.palette.chip4.main,
        color: theme.palette.chipText.main,
   
}));
export const StatusVoid = styled(Chip)(({theme})=>({
        borderRadius: '8px',
        padding:'2px 0',
        fontSize: '.8rem',
        width:'88px',
        background:theme.palette.chip5.main,
        color: theme.palette.chipText.main,
   
}));