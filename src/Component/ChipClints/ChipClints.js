import React from "react";
import { StatusVip, StatusRegular, StatusNew,StatusVoid,StatusUpadid } from "./styles";
export default function ChipClints({ type }) {
  let data;
  
  switch (type) {
    case "VIP":
      data = {
        tagname: <StatusVip label="VIP" />,
      };
      break;
    case "Regular":
      data = {
        tagname: <StatusRegular label="Regular" />,
      };
      break;
    case "New":
      data = {
        tagname: <StatusNew label="New" />,
      };
      break;

    case "Refound":
      data = {
        tagname: <StatusVip label="Refound" />,
      };
      break;
    case "Unpaid":
      data = {
        tagname: <StatusUpadid label="Unpaid" />,
      };
      break;
    case "Paid":
      data = {
        tagname: <StatusNew label="Paid" />,
      };
      break;
    case "Void":
      data = {
        tagname: <StatusVoid label="Void" />,
      };
      break;
    case "Exchange":
      data = {
        tagname: <StatusVip label="Exchange" />,
      };
    case "Blocked":
      data = {
        tagname: <StatusUpadid label="Blocked" />,
      };
      break;

    default:
      break;
  }
  
  return <>{data.tagname}</>;
}
