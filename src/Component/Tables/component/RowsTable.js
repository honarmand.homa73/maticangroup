import { TableBody, TableCell, TableRow } from "@mui/material";
import React from "react";
import { Link} from "react-router-dom";

const RowsTable = ({ headers, data,Links }) => {
  return (
    <TableBody>
      <>
        {data.map((row) => (
          <TableRow component={Link} to={Links} sx={{textDecoration:'none'}}>
            {headers.map((head) => (
              <TableCell>{row[head.key]}</TableCell>
            ))}
          </TableRow>
        ))}
      </>
    </TableBody>
  );
};

export default RowsTable;