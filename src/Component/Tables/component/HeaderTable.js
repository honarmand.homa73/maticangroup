import {TableHead, TableRow } from "@mui/material";
import React from "react";
import {TableCellHeader} from "./styles"
const HeaderTable = ({ headers }) => {
  return (
    <TableHead>
      <TableRow>
        {headers.map((item) => {
          return <TableCellHeader key={item.key}>{item.name}</TableCellHeader>;
        })}
      </TableRow>
    </TableHead>
  );
};

export default HeaderTable;