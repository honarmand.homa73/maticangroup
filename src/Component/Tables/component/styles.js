import { styled } from "@mui/material/styles";
import { TableCell } from "@mui/material";

export const TableCellHeader = styled(TableCell)({
    fontStyle: "normal",
    fontWeight: "700",
    fontSize: "14px",
    lineHeight: "20px",
  
});