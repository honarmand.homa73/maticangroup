import { Table, TableContainer, TableHead, TableRow } from "@mui/material";
import React from "react";
import HeaderTable from "./component/HeaderTable";
import RowsTable from "./component/RowsTable";


const Tables = (props) => {
  const { headers, data,Link } = props;
  return (
    <TableContainer sx={{ backgroundColor: "#FFFFFF", mt: 3 }}>
      <Table>
        <HeaderTable headers={headers} />
        <RowsTable data={data} headers={headers} Links={Link}/>
      </Table>
    </TableContainer>
  );
};

export default Tables;